package com.coderman.model.meta.service;

import com.coderman.model.meta.bean.PlantUMLApiContextBean;
import com.coderman.model.meta.bean.back.ApiBean;
import com.coderman.model.meta.bean.back.FieldBean;
import com.coderman.model.meta.bean.back.TemplateParseContextBean;

/**
 * Description:
 * date: 2022/5/12
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public interface FormElementFactory {

    /**
     * 构建插入表单元数据信息
     * @return
     */
    String buildInsertForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean);

    /**
     * 构建修改表单元数据信息
     * @return
     */
    String buildUpdateForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean);


    /**
     * 构建详情表单元数据信息
     * @return
     */
    String buildDetailForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean);


    /**
     * 构建分页表单元数据信息
     * @return
     */
    String buildPageForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean);

    /**
     * 在构建具体表单属性时，可以根据不同web框架动态配置属性对应的表单元素，实现根据框架来
     * @param fieldBean
     * @return
     */
    String preBuildComponent(FieldBean fieldBean);

}
