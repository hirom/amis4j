package com.coderman.model.meta.bean.back;

import com.coderman.model.meta.enums.VisibilityEnum;
import lombok.Data;

/**
 * Description:
 * date: 2022/1/21
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class FieldBean {
    /**
     * 属性名称
     */
    private String fieldName;

    /**
     * 属性描述
     */
    private String fieldDesc;

    /**
     * 属性中文注释
     */
    private String fieldDoc;

    /**
     * 属性类型
     * 1.基本数据类型
     * 2.列表对象
     * 3.单个对象
     * 4.数组
     */
    private String fieldType;

    private boolean extendFieldTag = false;


    /**
     * 属性的扩展信息模型
     */
    private FieldExtendBean fieldExtendBean;

    /**
     * 构建属性注释
     * @param desc
     */
    public void buildDesc(String desc){
        if(desc.startsWith(VisibilityEnum.PUBLIC.getTag())
                || desc.startsWith(VisibilityEnum.PRIVATE.getTag())
                || desc.startsWith(VisibilityEnum.PROTECT.getTag())){
            String newDesc = desc.substring(1,desc.length()-1);
            this.setFieldDesc(newDesc);
        }else {
            this.setFieldDesc(desc);
        }
    }



    private boolean isEmail(){
        return this.fieldName.toLowerCase().contains("email") && fieldDesc.equals("邮箱");
    }

    private boolean isPassWord(){
        return (this.fieldName.toLowerCase().contains("pass") || this.fieldName.contains("word")) && fieldDesc.equals("密码");
    }

    private boolean isURL(){
        return  this.fieldName.toLowerCase().contains("url") ||  fieldDesc.equals("链接");
    }

    /**
     * 判断是否是枚举类型
     * @return
     */
    private boolean isEnumData(){
        return  this.fieldName.toLowerCase().contains("type") ||  fieldDesc.equals("类型");
    }




}
