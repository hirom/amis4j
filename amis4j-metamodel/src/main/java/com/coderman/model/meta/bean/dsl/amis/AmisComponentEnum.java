package com.coderman.model.meta.bean.dsl.amis;

import com.coderman.model.meta.bean.dsl.ComponentDefine;

import java.util.Set;

/**
 * Description:百度amis comp类型枚举
 * date: 2022/4/7
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public enum AmisComponentEnum implements ComponentDefine {
    INPUT_TEXT("input-text","type","输入框",null),
    INPUT_DATE("input-date","type","日期选择器",null),
    INPUT_DATE_TIME("input-datetime","type","日期选择器",null),

    INPUT_EMAIL("input-email","type","邮箱输入框",null),
    INPUT_PASSWORD("input-password","type","密码输入框",null),
    TEXT("text","type","文本框",null),
    CHECK_BOX("checkbox","type","勾选框",null),
    SELECT("select","type","下拉框",null),
    JSON_EDITOR("json-editor","type","json编辑器",null),

    ;

    private String compName;

    private String compFieldName;

    private String compDesc;

    private Set<String> supportFieldSet;


    AmisComponentEnum(String compName, String compFieldName, String compDesc, Set<String> supportFieldSet){
        this.compName = compName;
        this.compFieldName = compFieldName;
        this.compDesc = compDesc;
        this.supportFieldSet = supportFieldSet;
    }

    public String getCompName() {
        return this.compName;
    }

    public String getCompFieldName() {
        return this.compFieldName;
    }

    public String getCompDesc() {
        return this.compDesc;
    }

    public Set<String> getSupportFieldSet() {
        return this.supportFieldSet;
    }

    public static AmisComponentEnum check(String compName) {
        for (AmisComponentEnum amisComponentEnum : AmisComponentEnum.values()){
            if(amisComponentEnum.getCompName().equals(compName)){
                return amisComponentEnum;
            }
        }
        return null;
    }


}
