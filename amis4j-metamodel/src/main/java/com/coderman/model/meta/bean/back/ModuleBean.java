package com.coderman.model.meta.bean.back;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * Description:项目模块
 * date: 2022/1/21
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class ModuleBean {

    private Long id;

    /**  项目编码 **/
    private String projectCode;

    /**
     * 模块名称
     */
    private String moduleCode;

    /**
     * 模块描述:xxx管理
     */
    private String moduleDesc;

    /**  上下文名称
     * xxx
     * **/

    private String contextName;

    /**
     * 子模块路由访问URL
     */
    private String routeUrl;

    /**
     * 子模块列表
     */
    List<ModuleBean> subModuleBeanList;


    /**
     * api列表
     */
    private List<ApiBean> apiBeanList;

    /**  创建时间 **/
    private Date dateCreate;
    /**  修改时间 **/
    private Date dateUpdate;
    /**  修改人 **/
    private Long updateUserId;
    /**  创建人 **/
    private Long createUserId;

    private String webAppName;


    public static ModuleBean getInstance(String projectCode, String moduleCode, String moduleDesc){
        ModuleBean moduleBean = new ModuleBean();
        moduleBean.setCreateUserId(1L);
        moduleBean.setDateCreate(new Date());
        moduleBean.setUpdateUserId(1L);
        moduleBean.setDateUpdate(new Date());
        moduleBean.setRouteUrl("");
        moduleBean.setProjectCode(projectCode);
        moduleBean.setModuleCode(moduleCode);
        moduleBean.setContextName("");
        moduleBean.setModuleDesc(moduleDesc);
        return moduleBean;
    }
}
