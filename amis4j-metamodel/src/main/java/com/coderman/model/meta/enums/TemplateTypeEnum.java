package com.coderman.model.meta.enums;

/**
 * Description:
 * date: 2022/5/10
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public enum TemplateTypeEnum {
    CONTAINER(1,"容器级别"),
    MODULE(2,"模块级别"),
    ;
    private Integer code;
    private String desc;

    TemplateTypeEnum(Integer code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
