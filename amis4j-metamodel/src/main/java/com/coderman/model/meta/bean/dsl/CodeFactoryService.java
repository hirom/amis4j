package com.coderman.model.meta.bean.dsl;

import com.coderman.model.meta.bean.PlantUMLApiContextBean;
import com.coderman.model.meta.bean.back.FieldBean;
import com.coderman.model.meta.bean.container.ContainerBean;

/**
 * Description:
 * date: 2022/4/29
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public interface CodeFactoryService {
    /**
     * 构建web前端代码的入口
     * 默认增，删，改，查容器，api级别
     * @param containerBean
     * @return
     */
    String buildWebCode(ContainerBean containerBean, PlantUMLApiContextBean plantUMLApiContextBean);


    /**
     * 在构建具体表单属性时，可以根据不同web框架动态配置属性对应的表单元素，实现根据框架来
     * @param fieldBean
     * @return
     */
    String preBuildComponent(FieldBean fieldBean);


}
