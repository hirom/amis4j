package com.coderman.model.meta.bean.container;

import com.coderman.model.meta.bean.dsl.ComponentDefine;
import com.coderman.utils.kvpair.KVPair;

import java.util.List;

/**
 * Description:
 * date: 2022/4/7
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class ComponentBean {
    /**
     * 组件信息
     */
    private ComponentDefine comp;
    /**
     * 组件其他属性kv列表
     */
    private List<KVPair> kvPairList;
    /**
     * 组件api数据模型名称
     */
    private String name;
    /**
     * 组件数据模型描述
     */
    private String label;

    /**
     * 构建json内容
     */
    public String buildJsonStr(){
        StringBuilder builder = new StringBuilder();
        builder.append("\""+comp.getCompFieldName()+"\""+": \""+comp.getCompName()+"\",");
        builder.append("\"name\": \""+this.name+"\",");
        builder.append("\"label\": \""+this.label+"\"");
        if(this.kvPairList != null && !this.kvPairList.isEmpty()){
            for (KVPair kvPair : this.kvPairList){
                builder.append("\""+kvPair.getK()+"\": \""+kvPair.getV()+"\",");
            }
            return builder.substring(0,builder.length() - 1);
        }
        return builder.toString();
    }


    public ComponentBean(){}

    public ComponentBean(String name, String label, ComponentDefine comp){
        this.name = name;
        this.label = label;
        this.comp = comp;
    }


    public ComponentDefine getComp() {
        return comp;
    }

    public List<KVPair> getKvPairList() {
        return kvPairList;
    }

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }

    public void setComp(ComponentDefine comp) {
        this.comp = comp;
    }

    public void setKvPairList(List<KVPair> kvPairList) {
        this.kvPairList = kvPairList;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
