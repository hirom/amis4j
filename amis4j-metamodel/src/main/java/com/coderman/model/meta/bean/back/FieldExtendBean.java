package com.coderman.model.meta.bean.back;

import com.coderman.utils.kvpair.KVPair;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * Description:
 * date: 2022/5/12
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class FieldExtendBean {

    /**
     * 表单元素形式
     * 输入框
     * 文本框
     * 单选 多选等
     */
    private String inputType;

    /**
     * 是否必填
     */
    private boolean required;

    /**
     * 最长输入
     */
    private Long maxLength;

    /**
     * 最短输入
     */
    private Long minLength = 2L;

    /**
     * 是否可以在列表中搜索
     */
    private boolean searchInPage;

    /**
     * 是否可以在列表中进行排序
     */
    private boolean sortInPage;

    /**
     * 如果是单选或者下拉的默认值列表
     * 关联到文档中的enum
     */
    private List<KVPair> defaultValueList;


    /**
     * 字段在数据库中的长度
     */
    private Integer dbColumnLength;

    /**
     * 字段在数据库中数据存储类型
     */
    private String dbColumnType;

    /**
     * api参数属性是否在数据库中存在
     */
    private boolean isReferDBModel;

    /**
     * 字段所属分组标示
     */
    private String groupCode;


    /**
     * 字段的文档顺序
     */
    private String index;

    /**
     * 是否需要在插入表单form中渲染
     */
    private boolean inInsertForm;


    /**
     * 是否需要在修改表单form中渲染
     */
    private boolean inUpdateForm;

    /**
     * 是否需要在详情表单form中渲染
     */
    private boolean inDetailForm;


    /**
     * 是否需要在列表form中渲染
     */
    private boolean inPageForm;

}
