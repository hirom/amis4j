package com.coderman.model.meta.bean.back;

import com.coderman.model.meta.enums.CodeFileTypeEnum;
import lombok.Data;

/**
 * Description:前端框架代码模版配置
 * date: 2022/5/9
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class WebCodeTemplateBean {
    private Long id;
    /**
     * 前端web框架名称
     * amis,vform,ant-design
     */
    private String webArchName;
    /**
     * 模版名称
     */
    private String templateName;
    /**
     * 模版内容
     */
    private String templateContent;
    /**
     * 模版类型
     * api级别
     * 模块级别
     * 容器级别
     */
    private Integer templateType;


    /**
     * 模版描述和使用场景
     *
     */
    private String desc;


    /**
     * 引用此模版的模块编码
     */
    private String moduleCode;


    /**
     * 输出代码文件格式
     */
    private String outCodeType;


    /**
     * 模版文件的内容格式
     */
    private String contentType;


    /**
     * 模版文件类型描述
     */
    private String templateTypeDesc;

    public WebCodeTemplateBean(){}

    public WebCodeTemplateBean(String templateName){
        this.templateName = templateName;
    }

    public static WebCodeTemplateBean getInstance(String templateName){
        return new WebCodeTemplateBean(templateName);
    }


    public boolean isFtl(){
        return this.contentType.equals(CodeFileTypeEnum.FREEMARKER.getCodeType());
    }
}
