package com.coderman.model.meta.enums;

/**
 * Description:模版元素枚举类
 * date: 2022/5/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public enum TemplateEleEnum {
    /**
     * 文本信息
     */
    PROJECT_DEV_URL("projectDevUrl","项目开发环境链接","$projectDevUrl","http://localhost.dev.com"),
    PROJECT_TEST_URL("projectTestUrl","项目测试环境链接","$projectTestUrl","http://localhost.test.com"),
    PROJECT_PRE_URL("projectPreUrl","项目预发环境链接","$projectPreUrl","http://localhost.pre.com"),
    PROJECT_PRO_URL("projectProUrl","项目生产环境链接","$projectProUrl","http://localhost.pro.com"),

    MODULE_ROUTE_URL("moduleRouteUrl","模块路由链接","$moduleRouteUrl","/user/manager"),
    MODULE_TITLE("moduleTitle","模块标题","$moduleTitle","用户管理"),
    MODULE_CODE("moduleCode","模块编码","$moduleCode","user"),
    MODULE_CONTEXT("moduleContext","模块上下文名称","$moduleContext","userContext"),
    MODULE_LIST("moduleList","模块信息列表","$moduleList",""),
    INSERT_API_TITLE("insertApiTitle","保存api标题","$insertApiTitle","新增用户信息"),
    UPDATE_API_TITLE("updateApiTitle","修改api标题","$updateApiTitle","修改用户信息"),
    DETAIL_API_TITLE("detailApiTitle","查询api标题","$detailApiTitle","查看用户详情"),
    /**
     * 字段元数据
     */
    INSERT_FORM("insertForm","插入-表单元素内容","$insertForm",""),
    UPDATE_FORM("updateForm","修改-表单元素内容","$updateForm",""),
    DETAIL_FORM("detailForm","查看-表单元素内容","$detailForm",""),
    PAGE_FORM("pageForm","分页列表-表单元素内容","$pageForm",""),
    /**
     * api信息
     */
    INSERT_API("insertApi","插入的api资源","$insertApi","/user/save"),
    UPDATE_API("updateApi","修改的api资源","$updateApi","/user/update"),
    DETAIL_API("detailApi","获取单条数据的api资源","$detailApi","/user/getById"),

    PAGE_API("pageApi","分页列表查询的api资源","$pageApi","/user/page"),
    DELETE_API("deleteApi","删除的api资源","$deleteApi","/user/deleteById"),
    DELETE_BATCH_API("deleteBatchApi","批量删除的api资源","$deleteBatchApi","/user/deleteBatch"),
    UPDATE_BATCH_API("updateBatchApi","批量修改的api资源","$updateBatchApi","/user/updateBatch"),
    LIST_API("listApi","列表查询api资源","$listApi","/user/list"),
    EXPORT_API("exportApi","数据导出api资源","$exportApi","/user/export"),
    IMPORT_API("importApi","数据数据导入api资源","$importApi","/user/import"),


    INSERT_API_PARAM("insertApiParam","插入api参数","$insertApiParam","{\n" +
            "\t\t\t\"type\": \"input-text\",\n" +
            "\t\t\t\"name\": \"chineseName\",\n" +
            "\t\t\t\"label\": \"用户姓名\",\n" +
            "            \"required\": true\n" +
            "\t\t\t\n" +
            "\t\t}, {\n" +
            "\t\t\t\"type\": \"input-text\",\n" +
            "\t\t\t\"name\": \"age\",\n" +
            "\t\t\t\"label\": \"用户年龄\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"type\": \"input-text\",\n" +
            "\t\t\t\"name\": \"phone\",\n" +
            "\t\t\t\"label\": \"电活\",\n" +
            "            \"required\": true\n" +
            "\t\t}"),
    UPDATE_API_PARAM("updateApiParam","修改api参数","$updateApiParam","{\n" +
            "\t\t\t\"type\": \"text\",\n" +
            "\t\t\t\"name\": \"chineseName\",\n" +
            "\t\t\t\"label\": \"用户姓名\",\n" +
            "            \"required\": true\n" +
            "\t\t\t\n" +
            "\t\t}, {\n" +
            "\t\t\t\"type\": \"input-text\",\n" +
            "\t\t\t\"name\": \"age\",\n" +
            "\t\t\t\"label\": \"用户年龄\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"type\": \"input-text\",\n" +
            "\t\t\t\"name\": \"phone\",\n" +
            "\t\t\t\"label\": \"电活\",\n" +
            "            \"required\": true\n" +
            "\t\t}"),
    DETAIL_API_PARAM("detailApiParam","单条详情api参数","$detailApiParam","{\n" +
            "\t\t\t\"type\": \"text\",\n" +
            "\t\t\t\"name\": \"chineseName\",\n" +
            "\t\t\t\"label\": \"用户姓名\"\n" +
            "\t\t\t\n" +
            "\t\t}, {\n" +
            "\t\t\t\"type\": \"text\",\n" +
            "\t\t\t\"name\": \"age\",\n" +
            "\t\t\t\"label\": \"用户年龄\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"type\": \"text\",\n" +
            "\t\t\t\"name\": \"phone\",\n" +
            "\t\t\t\"label\": \"电活\"\n" +
            "\t\t}"),
    PAGE_API_PARAM("pageApiParam","分页列表详情api参数","$pageApiParam","{\n" +
            "\t\t\t\"type\": \"text\",\n" +
            "\t\t\t\"name\": \"chineseName\",\n" +
            "\t\t\t\"label\": \"用户姓名\",\n" +
            "\t\t\t\"searchable\": {\n" +
            "\t\t\t\t\"type\": \"input-text\",\n" +
            "\t\t\t\t\"name\": \"chineseName\",\n" +
            "\t\t\t\t\"placeholder\": \"输入用户姓名\"\n" +
            "\t\t\t}\n" +
            "\t\t}, {\n" +
            "\t\t\t\"type\": \"text\",\n" +
            "\t\t\t\"name\": \"age\",\n" +
            "\t\t\t\"label\": \"用户年龄\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"type\": \"text\",\n" +
            "\t\t\t\"name\": \"phone\",\n" +
            "\t\t\t\"label\": \"电活\"\n" +
            "\t\t}"),
    LIST_API_PARAM("listApiParam","列表详情api参数","$listApiParam","{\n" +
            "\t\t\t\"type\": \"text\",\n" +
            "\t\t\t\"name\": \"chineseName\",\n" +
            "\t\t\t\"label\": \"用户姓名\"\n" +
            "\t\t\t\n" +
            "\t\t}, {\n" +
            "\t\t\t\"type\": \"text\",\n" +
            "\t\t\t\"name\": \"age\",\n" +
            "\t\t\t\"label\": \"用户年龄\"\n" +
            "\t\t}, {\n" +
            "\t\t\t\"type\": \"text\",\n" +
            "\t\t\t\"name\": \"phone\",\n" +
            "\t\t\t\"label\": \"电活\"\n" +
            "\t\t}"),

    ;
    /**
     * 元素标示
     */
    private String code;
    /**
     * 元素说明
     */
    private String desc;

    /**
     * 元素变量名
     */
    private String varCode;

    /**
     * 数据案例
     */
    private String demo;

    TemplateEleEnum(String code, String desc, String varCode, String demo){
        this.code = code;
        this.desc = desc;
        this.varCode = varCode;
        this.demo = demo;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public String getVarCode() {
        return varCode;
    }

    public String getDemo() {
        return demo;
    }
}
