package com.coderman.model.meta;

import java.util.HashSet;
import java.util.Set;

/**
 * Description:
 * date: 2021/7/12
 *
 * @author shenshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public class GlobalConstant {
    /**
     * 以!开头的则忽略解析
     */
    public static final String PLANT_DOC_IGNORE = "!";

    /**
     * plantUML的API类描述
     */
    public static final String API = "api";


    public static final String DTO = "dto";

    public static final String VO = "vo";

    public static final String ENUM = "enum";

    /**
     * 存放api文档的地方
     */
    public static final String PLANT_UML_FILE_DIR = "webapp-model";

    /**
     * 需要在列表中搜索的属性集合
     */
    public static final String SEARCH_KEY_LIST = "searchKeyList";


    /**
     * 不需要必填的的属性集合
     */
    public static final String NOT_REQUIRED_KEY_LIST = "notRequireKeyList";

    /**
     * 如果某个api集合符合模块级别的代码模版配置则可以配置对应的json模版，通过对应的变量占位符号和对应实现进行渲染
     */
    public static final String TEMPLATE_JSON_KEY = "templatejsonkey";


    /**
     * 在表单插入场景下不需要与表单元素绑定，起到过滤和个性化设计的作用
     */
    public static final String EXCLUDE_INSERT = "excludeInsert";


    /**
     * 在表单修改场景下不需要与表单元素绑定，起到过滤和个性化设计的作用
     */
    public static final String EXCLUDE_UPDATE = "excludeUpdate";

    /**
     * 在列表渲染场景下不需要与表单元素绑定，起到过滤和个性化设计的作用
     */
    public static final String EXCLUDE_PAGE = "excludePage";

    /**
     * 在列表渲染场景下不需要与表单元素绑定，起到过滤和个性化设计的作用
     */
    public static final String EXCLUDE_DETAIL = "excludeDetail";


    /**
     * 获取内置扩展字段集合
     * @return
     */
    public static Set<String> getInnerKey(){
        Set<String> innerKeySet = new HashSet<>();
        innerKeySet.add(EXCLUDE_DETAIL);
        innerKeySet.add(EXCLUDE_PAGE);
        innerKeySet.add(EXCLUDE_UPDATE);
        innerKeySet.add(EXCLUDE_INSERT);
        innerKeySet.add(NOT_REQUIRED_KEY_LIST);
        innerKeySet.add(TEMPLATE_JSON_KEY);
        innerKeySet.add(SEARCH_KEY_LIST);

        return innerKeySet;
    }



}

