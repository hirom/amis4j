package com.coderman.model.meta.bean.dsl.amis;

import com.coderman.model.meta.bean.dsl.ContainerDefine;

/**
 * Description:amis容器类型
 * 对应template/amis/*下的json模板
 * date: 2022/4/7
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public enum AmisContainerEnum implements ContainerDefine {
    INSERT_FORM("insertform","表单新建容器"),
    UPDATE_FORM("updateform","表单修改容器"),
    DETAIL_FORM("detailform","表单查看容器"),
    PAGELIST("pagelist","列表容器"),

    ;


    private String name;

    private String desc;

    AmisContainerEnum(String name, String desc){
        this.name = name;
        this.desc = desc;
    }

    @Override
    public String getContainerName() {
        return this.name;
    }

    @Override
    public String getContainerDesc() {
        return this.name;
    }


    public static boolean isInsertForm(String containerName){
        return AmisContainerEnum.INSERT_FORM.getContainerName().equals(containerName);
    }


    public static boolean isUpdateForm(String containerName){
        return AmisContainerEnum.UPDATE_FORM.getContainerName().equals(containerName);
    }

    public static boolean isDetailForm(String containerName){
        return AmisContainerEnum.DETAIL_FORM.getContainerName().equals(containerName);
    }


    public static boolean isPageList(String containerName){
        return AmisContainerEnum.PAGELIST.getContainerName().equals(containerName);
    }

}
