package com.coderman.model.meta.bean.back;

import lombok.Data;

import java.util.Date;

/**
 * Description:项目描述
 * date: 2022/1/21
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class ProjectBean {

    /**  项目应用名 **/
    private String projectCode;
    /**  应用描述 **/
    private String projectDesc;
    /**   业务领域编码 **/
    private String domainCode;
    /**   业务领域描述 **/
    private String domainDesc;

    /**
     * app描述
     */
    private String appDesc;

    private String testUrl;

    private String devUrl;

    private String preUrl;

    private String proUrl;

    /**
     * 布局类型
     */
    private Integer layoutType;
    /**  创建时间 **/
    private Date dateCreate;
    /**  修改时间 **/
    private Date dateUpdate;
    /**  修改人 **/
    private Long updateUserId;
    /**  创建人 **/
    private Long createUserId;

    /**  使用的web框架 **/
    private String webDsl;

    /**  项目git地址 **/
    private String gitAddress;

    private Long id;

    /**
     * 使用的dsl框架名称
     */
    private String dslAppName;

    /**
     * 是否立即进行代码生成
     */
    private boolean createCode;
}
