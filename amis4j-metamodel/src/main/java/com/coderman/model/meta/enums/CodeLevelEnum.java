package com.coderman.model.meta.enums;

/**
 * Description:生成的代码内容级别
 * date: 2022/5/12
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public enum CodeLevelEnum {
    API("API"),
    MODULE("MODULE"),
    ;

    private String level;
    CodeLevelEnum(String level){
        this.level = level;
    }

    public String getLevel() {
        return level;
    }
}
