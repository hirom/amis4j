package com.tianhua.amis4j.infrast.mapper;

import java.util.List;

import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.infrast.dataobject.ModuleConfigDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * @Description:模块配置mapperDAO接口
 * @Author:shenshuai
 * @CreateTime:2022-04-15 16:23:32
 * @version v1.0
 */
@Mapper
public interface ModuleConfigMapper{

	/**
	 * 
	 * @Description:新增或修改
	 * @param moduleConfigDO
	 * @return int
	 */
	public long insert(ModuleConfigDO moduleConfigDO);

	/**
	 * @Description: 通过id删除数据
	 * @param id
	 * @return int
	 */
	public int deleteById(Long id);

	/**
	 * @Description: 通过id查询
	 * @param id
	 * @return ResultDataDto<ModuleConfigDO>
	 */
	public ModuleConfigDO getById(Long id);

	/**
	 * @Description:查询所有数据
	 * @return List<ModuleConfigDO
	 */
	public List<ModuleConfigDO>  getAll(@Param(value = "content") String content);


	public List<ModuleConfigDO> getByProjectCode(String projectCode);

	/**
	*
	* @Description:新增或修改
	* @param moduleConfigDO
	* @return int
	*/
	public int update(ModuleConfigDO moduleConfigDO);

	/**
	 * @Description:查询所有数据
	 * @return List<ModuleConfigDO
	 */
	 public List<ModuleConfigDO>  getPageList(@Param(value = "page") PageBean page);


	/**
	 * @Description:查询数量
	 * @return int
	 */
	 public int  getPageCount(@Param(value = "page") PageBean page);


}