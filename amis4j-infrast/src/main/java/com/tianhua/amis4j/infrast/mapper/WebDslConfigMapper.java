package com.tianhua.amis4j.infrast.mapper;

import java.util.List;

import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.infrast.dataobject.WebDslConfigDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * @Description:项目配置mapperDAO接口
 * @Author:shenshuai
 * @CreateTime:2022-04-15 16:23:32
 * @version v1.0
 */
@Mapper
public interface WebDslConfigMapper{

	/**
	 * 
	 * @Description:新增或修改
	 * @param webDslConfigDO
	 * @return int
	 */
	public long insert(WebDslConfigDO webDslConfigDO);

	/**
	 * @Description: 通过id删除数据
	 * @param id
	 * @return int
	 */
	public int deleteById(Long id);

	/**
	 * @Description: 通过id查询
	 * @param id
	 * @return ResultDataDto<WebDslConfigDO>
	 */
	public WebDslConfigDO getById(Long id);

	/**
	 * @Description:查询所有数据
	 * @return List<WebDslConfigDO
	 */
	public List<WebDslConfigDO>  getAll(@Param(value = "query") WebDslConfigDO query);


	/**
	 * @Description:查询所有数据
	 * @return List<WebDslConfigDO
	 */
	public List<WebDslConfigDO>  selectByProjectCode(String projectCode);

	/**
	*
	* @Description:新增或修改
	* @param webDslConfigDO
	* @return int
	*/
	public int update(WebDslConfigDO webDslConfigDO);

	/**
	 * @Description:查询所有数据
	 * @return List<WebDslConfigDO
	 */
	 public List<WebDslConfigDO>  getPageList(@Param(value = "page") PageBean pageBean);


	/**
	 * @Description:查询数量
	 * @return int
	 */
	 public int  getPageCount(@Param(value = "page") PageBean pageBean);


}