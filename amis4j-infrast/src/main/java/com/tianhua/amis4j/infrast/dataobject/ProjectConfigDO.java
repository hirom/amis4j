package com.tianhua.amis4j.infrast.dataobject;

import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;
import lombok.ToString;

 /**
 * @Description:项目配置DO类
 * @Author:shenshuai
 * @CreateTime:2022-04-15 16:23:32
 * @version v1.0
 */
@Data
@ToString
public class ProjectConfigDO{


	/**   主键 **/
	private Long id;

	/**  项目应用名 **/
	private String projectCode;

	/**  应用描述 **/
	private String projectDesc;

	/**   业务领域编码 **/
	private String domainCode;

	/**   业务领域描述 **/
	private String domainDesc;

	/**  测试环境域名 **/
	private String testUrl;

	/**  测试环境域名 **/
	private String devUrl;

	/**  测试环境域名 **/
	private String preUrl;

	/**  测试环境域名 **/
	private String proUrl;

	/**  使用的web框架 **/
	private String webDsl;

	/**  项目git地址 **/
	private String gitAddress;

	/**  创建时间 **/
	private Date dateCreate;

	/**  修改时间 **/
	private Date dateUpdate;

	/**  修改人 **/
	private Long updateUserId;

	/**  创建人 **/
	private Long createUserId;

}