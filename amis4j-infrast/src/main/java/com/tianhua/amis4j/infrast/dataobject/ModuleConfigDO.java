package com.tianhua.amis4j.infrast.dataobject;

import java.util.Date;
import lombok.Data;
import lombok.ToString;

 /**
 * @Description:模块配置DO类
 * @Author:shenshuai
 * @CreateTime:2022-04-15 16:23:32
 * @version v1.0
 */
@Data
@ToString
public class ModuleConfigDO{


	/**   主键 **/
	private Long id;

	/**  项目名称 **/
	private String projectCode;

	/**  项目应用名 **/
	private String moduleCode;

	/**  应用描述 **/
	private String moduleDesc;

	/**   所属上下文名称 **/
	private String contextName;

	/**  模块路由url **/
	private String routeUrl;

	/**  创建时间 **/
	private Date dateCreate;

	/**  修改时间 **/
	private Date dateUpdate;

	/**  修改人 **/
	private Long updateUserId;

	/**  创建人 **/
	private Long createUserId;

}