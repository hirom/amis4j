package com.tianhua.amis4j.infrast.dataobject;

import java.util.Date;
import java.math.BigDecimal;
import lombok.Data;
import lombok.ToString;

 /**
 * @Description:dto/vo参数表DO类
 * @Author:shenshuai
 * @CreateTime:2022-04-15 16:23:32
 * @version v1.0
 */
@Data
@ToString
public class WebApiParamDO{


	/**   主键 **/
	private Long id;

	/**  项目名称 **/
	private String projectCode;

	/**  项目应用名 **/
	private String moduleCode;

	/**  参数类名 **/
	private String paramClassName;

	/**  参数类描述 **/
	private String paramClassDesc;

	/**  参数属性集合json **/
	private String paramFieldJson;

	/**  创建时间 **/
	private Date dateCreate;

	/**  修改时间 **/
	private Date dateUpdate;

	/**  修改人 **/
	private Long updateUserId;

	/**  创建人 **/
	private Long createUserId;

}