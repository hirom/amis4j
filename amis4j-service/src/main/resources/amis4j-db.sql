CREATE TABLE `project_config`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '' 主键 '',
    `project_code`   varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 项目应用名 '',
    `project_desc`   varchar(500) NOT NULL DEFAULT '''' COMMENT '' 应用描述 '',
    `domain_code`    varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 业务领域编码 '',
    `domain_desc`    varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 业务领域描述 '',
    `test_url`       varchar(128) NOT NULL DEFAULT '''' COMMENT '' 测试环境域名 '',
    `dev_url`        varchar(128) NOT NULL DEFAULT '''' COMMENT '' 测试环境域名 '',
    `pre_url`        varchar(128) NOT NULL DEFAULT '''' COMMENT '' 测试环境域名 '',
    `pro_url`        varchar(128) NOT NULL DEFAULT '''' COMMENT '' 测试环境域名 '',
    `web_dsl`        varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 使用的web框架 '',
    `git_address`    varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 项目git地址 '',
    `date_create`    timestamp    NOT NULL DEFAULT ''2000 - 01 - 01 00:00:00 '' COMMENT '' 创建时间 '',
    `date_update`    timestamp    NOT NULL DEFAULT ''2000 - 01 - 01 00:00:00 '' COMMENT '' 修改时间 '',
    `update_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '' 修改人 '',
    `create_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '' 创建人 '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4 COMMENT = ''项目配置'';


CREATE TABLE `module_config`
(
    `id`             bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '' 主键 '',
    `project_code`   varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 项目名称 '',
    `module_code`    varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 项目应用名 '',
    `module_desc`    varchar(500) NOT NULL DEFAULT '''' COMMENT '' 应用描述 '',
    `context_name`   varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 所属上下文名称 '',
    `route_url`      varchar(128) NOT NULL DEFAULT '''' COMMENT '' 模块路由url '',
    `date_create`    timestamp    NOT NULL DEFAULT ''2000 - 01 - 01 00:00:00 '' COMMENT '' 创建时间 '',
    `date_update`    timestamp    NOT NULL DEFAULT ''2000 - 01 - 01 00:00:00 '' COMMENT '' 修改时间 '',
    `update_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '' 修改人 '',
    `create_user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '' 创建人 '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4 COMMENT = ''模块配置'';


CREATE TABLE `web_api`
(
    `id`                 bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '' 主键 '',
    `project_code`       varchar(50)   NOT NULL DEFAULT '''' COMMENT '' 项目名称 '',
    `module_code`        varchar(50)   NOT NULL DEFAULT '''' COMMENT '' 项目应用名 '',
    `api_url`            varchar(500)  NOT NULL DEFAULT '''' COMMENT '' api信息 '',
    `api_doc`            varchar(500)  NOT NULL DEFAULT '''' COMMENT '' api描述 '',
    `method_type`        varchar(500)  NOT NULL DEFAULT '''' COMMENT '' 请求方式 '',
    `request_json_data`  varchar(2000) NOT NULL DEFAULT '''' COMMENT '' 请求json数据演示 '',
    `response_json_data` varchar(2000) NOT NULL DEFAULT '''' COMMENT '' 响应json数据演示 '',
    `request_param`      json COMMENT '' 请求参数元信息 '',
    `date_create`        timestamp     NOT NULL DEFAULT ''2000 - 01 - 01 00:00:00 '' COMMENT '' 创建时间 '',
    `date_update`        timestamp     NOT NULL DEFAULT ''2000 - 01 - 01 00:00:00 '' COMMENT '' 修改时间 '',
    `update_user_id`     bigint(20) NOT NULL DEFAULT 0 COMMENT '' 修改人 '',
    `create_user_id`     bigint(20) NOT NULL DEFAULT 0 COMMENT '' 创建人 '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4 COMMENT = ''API数据表'';


CREATE TABLE `web_api_param`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '' 主键 '',
    `project_code`     varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 项目名称 '',
    `module_code`      varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 项目应用名 '',
    `param_class_name` varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 参数类名 '',
    `param_class_desc` varchar(500) NOT NULL DEFAULT '''' COMMENT '' 参数类描述 '',
    `param_field_json` json COMMENT '' 参数属性集合json '',
    `date_create`      timestamp    NOT NULL DEFAULT ''2000 - 01 - 01 00:00:00 '' COMMENT '' 创建时间 '',
    `date_update`      timestamp    NOT NULL DEFAULT ''2000 - 01 - 01 00:00:00 '' COMMENT '' 修改时间 '',
    `update_user_id`   bigint(20) NOT NULL DEFAULT 0 COMMENT '' 修改人 '',
    `create_user_id`   bigint(20) NOT NULL DEFAULT 0 COMMENT '' 创建人 '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4 COMMENT = ''dto/vo参数表'';


CREATE TABLE `web_dsl_config`
(
    `id`               bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '' 主键 '',
    `project_code`     varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 项目编码 '',
    `module_code`      varchar(50)  NOT NULL DEFAULT '''' COMMENT '' 模块编码 '',
    `api_url`          varchar(128) NOT NULL DEFAULT '''' COMMENT '' api信息 '',
    `web_dsl_json`     text COMMENT '' api信息 '',
    `single_page_html` text COMMENT '' 单页面htmldemo '',
    `version`          varchar(128) NOT NULL DEFAULT '''' COMMENT '' 版本号 '',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 DEFAULT CHARSET = utf8mb4 COMMENT = ''项目配置'';


