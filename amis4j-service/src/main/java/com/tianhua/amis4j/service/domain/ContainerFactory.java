package com.tianhua.amis4j.service.domain;

import com.coderman.model.meta.bean.dsl.ContainerDefine;
import com.coderman.model.meta.bean.dsl.amis.AmisContainerEnum;
import com.coderman.model.meta.bean.dsl.vue.VueContainerEnum;
import com.tianhua.amis4j.service.enums.WebArchAppEnum;
import org.springframework.stereotype.Service;

/**
 * Description:
 * date: 2022/4/12
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Service
public class ContainerFactory {

    /**
     * 根据webArchName 获取列表容器
     * @param webArchName
     * @return
     */
    public ContainerDefine getPageListContainer(String webArchName){
        if(webArchName.equals(WebArchAppEnum.AMIS.getAppCode())){
            return AmisContainerEnum.PAGELIST;
        }
        else if(webArchName.equals(WebArchAppEnum.VUE_ELEMENT_UI.getAppCode())){
            return VueContainerEnum.PAGELIST;
        }
        return null;
    }

    /**
     * 根据webArchName 获取表单容器
     * @param webArchName
     * @return
     */
    public ContainerDefine getInsertFormContainer(String webArchName){
        if(webArchName.equals(WebArchAppEnum.AMIS.getAppCode())){
            return AmisContainerEnum.INSERT_FORM;
        }
        else if(webArchName.equals(WebArchAppEnum.VUE_ELEMENT_UI.getAppCode())){
            return VueContainerEnum.INSERT_FORM;
        }
        return null;
    }


    /**
     * 根据dslAppName 获取表单容器
     * @param webArchName
     * @return
     */
    public ContainerDefine getUpdateFormContainer(String webArchName){
        if(webArchName.equals(WebArchAppEnum.AMIS.getAppCode())){
            return AmisContainerEnum.UPDATE_FORM;
        }
        else if(webArchName.equals(WebArchAppEnum.VUE_ELEMENT_UI.getAppCode())){
            return VueContainerEnum.UPDATE_FORM;
        }
        return null;
    }


    /**
     * 根据dslAppName 获取表单容器
     * @param webArchName
     * @return
     */
    public ContainerDefine getDetailFormContainer(String webArchName){
        if(webArchName.equals(WebArchAppEnum.AMIS.getAppCode())){
            return AmisContainerEnum.DETAIL_FORM;
        }
        else if(webArchName.equals(WebArchAppEnum.VUE_ELEMENT_UI.getAppCode())){
            return VueContainerEnum.DETAIL_FORM;
        }
        return null;
    }


}
