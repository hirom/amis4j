package com.tianhua.amis4j.service.admin;

import com.coderman.model.meta.bean.back.TemplateEleVarBean;
import com.coderman.model.meta.bean.back.WebCodeTemplateBean;
import com.coderman.model.meta.bean.support.PageBean;
import com.coderman.model.meta.enums.TemplateEleEnum;
import com.coderman.model.meta.enums.TemplateTypeEnum;
import com.tianhua.amis4j.infrast.dataobject.WebArchTemplateDO;
import com.tianhua.amis4j.infrast.mapper.WebArchTemplateMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Description:
 * date: 2022/4/27
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Service
@Slf4j
public class WebCodeTemplateConfigService {

    @Resource
    private WebArchTemplateMapper webArchTemplateMapper;

    public void saveWebArchTemplateConfig(WebCodeTemplateBean webCodeTemplateBean){
        //写文件到项目目录中
        // 获取classpath
        String classpath = null;
        try {
            classpath = ResourceUtils.getURL("classpath:ftl").getPath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        WebArchTemplateDO webArchTemplateDO = new WebArchTemplateDO();
        BeanUtils.copyProperties(webCodeTemplateBean,webArchTemplateDO);
        webArchTemplateMapper.insert(webArchTemplateDO);


    }

    public void updateWebArchTemplateConfig(WebCodeTemplateBean webCodeTemplateBean){
        //写文件到项目目录中
        // 获取classpath
        String classpath = null;
        try {
            classpath = ResourceUtils.getURL("classpath:ftl").getPath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        WebArchTemplateDO webArchTemplateDO = new WebArchTemplateDO();
        BeanUtils.copyProperties(webCodeTemplateBean,webArchTemplateDO);
        webArchTemplateMapper.update(webArchTemplateDO);

        if(webCodeTemplateBean.isFtl()){
            String ftlFilePath = classpath + "/" + webCodeTemplateBean.getWebArchName()+"/"+webCodeTemplateBean.getTemplateName()+".ftl";
            log.info("准备写ftl模版内容到项目classpath目录下,ftlFilePath={}",ftlFilePath);
            try {
                FileUtils.write(new File(ftlFilePath),webCodeTemplateBean.getTemplateContent(),"utf-8");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public PageBean selectPage(PageBean pageBean){
        List<WebArchTemplateDO> webArchTemplateDOList = webArchTemplateMapper.getPageList(pageBean);
        List<WebCodeTemplateBean> webArchTemplateBeanList = new ArrayList<>();
        for (WebArchTemplateDO webArchTemplateDO : webArchTemplateDOList){
            WebCodeTemplateBean webCodeTemplateBean = new WebCodeTemplateBean();

            if(webArchTemplateDO.getTemplateType().intValue() == TemplateTypeEnum.CONTAINER.getCode().intValue()){
                webCodeTemplateBean.setTemplateTypeDesc(TemplateTypeEnum.CONTAINER.getDesc());
            }else{
                webCodeTemplateBean.setTemplateTypeDesc(TemplateTypeEnum.MODULE.getDesc());
            }

            BeanUtils.copyProperties(webArchTemplateDO,webCodeTemplateBean);
            webArchTemplateBeanList.add(webCodeTemplateBean);
        }
        pageBean.setCount(webArchTemplateMapper.getPageCount(pageBean));
        pageBean.setRows(webArchTemplateBeanList);
        return pageBean;
    }

    /**
     * 根据模版名称查询单条数据
     * @param templateName
     * @param archName
     * @return
     */
    public WebCodeTemplateBean getByTemplateNameAndArchName(String archName, String templateName){
        WebArchTemplateDO webArchTemplateDO = webArchTemplateMapper.getByTemplateNameAndArchName(archName, templateName);
        if(webArchTemplateDO == null){
            return null;
        }

        WebCodeTemplateBean webArchTemplateBean = new WebCodeTemplateBean();
        BeanUtils.copyProperties(webArchTemplateDO, webArchTemplateBean);
        return webArchTemplateBean;
    }


    public WebCodeTemplateBean getById(Long id){
        WebArchTemplateDO webArchTemplateDO = webArchTemplateMapper.getById(id);
        WebCodeTemplateBean webArchTemplateBean = new WebCodeTemplateBean();
        BeanUtils.copyProperties(webArchTemplateDO, webArchTemplateBean);
        return webArchTemplateBean;
    }


    public List<WebCodeTemplateBean> getByWebAppName(String webAppName){
        List<WebArchTemplateDO> webArchTemplateDOList = webArchTemplateMapper.getAll();
        List<WebArchTemplateDO> newList = webArchTemplateDOList.stream().filter(webArchTemplateDO -> webArchTemplateDO.getWebArchName().equals(webAppName)).collect(Collectors.toList());
        if(CollectionUtils.isEmpty(newList)){
            return null;
        }
        List<WebCodeTemplateBean> webArchTemplateBeanList = new ArrayList<>();
        for (WebArchTemplateDO webArchTemplateDO : newList){
            WebCodeTemplateBean webDslConfigBean = new WebCodeTemplateBean();
            BeanUtils.copyProperties(webArchTemplateDO,webDslConfigBean);
            webArchTemplateBeanList.add(webDslConfigBean);
        }
        return webArchTemplateBeanList;
    }


    /**
     * 获取模版支持的变量列表
     * @return
     */
    public List<TemplateEleVarBean> getTemplateVarList(){
        List<TemplateEleVarBean> eleVarBeanList = new ArrayList<>();
        for (TemplateEleEnum templateEleEnum : TemplateEleEnum.values()){
            TemplateEleVarBean eleVarBean = TemplateEleVarBean.TemplateEleVarBean(templateEleEnum.getCode(), templateEleEnum.getDesc());
            eleVarBean.setVarJson(templateEleEnum.getVarCode());
            eleVarBean.setVarFreemarker("${" + templateEleEnum.getCode() + "}");
            eleVarBean.setDemo(templateEleEnum.getDemo());
            eleVarBeanList.add(eleVarBean);
        }
        return eleVarBeanList;
    }




}
