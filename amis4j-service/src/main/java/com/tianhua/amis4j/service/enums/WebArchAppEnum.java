package com.tianhua.amis4j.service.enums;

/**
 * Description:
 * date: 2022/4/13
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public enum WebArchAppEnum {

    AMIS("amis"),
    VUE_ELEMENT_UI("vueelementui"),
    ANT_DESIGN("antdesign"),
    LayUI("layui"),

    ;
    private String appCode;
    WebArchAppEnum(String appCode){
        this.appCode = appCode;
    }


    public String getAppCode() {
        return appCode;
    }
}
