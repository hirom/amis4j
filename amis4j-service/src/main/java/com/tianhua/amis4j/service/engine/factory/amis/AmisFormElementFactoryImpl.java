package com.tianhua.amis4j.service.engine.factory.amis;

import com.coderman.model.meta.bean.PlantUMLApiContextBean;
import com.coderman.model.meta.bean.back.*;
import com.coderman.model.meta.bean.dsl.amis.AmisComponentEnum;
import com.coderman.model.meta.service.FormElementFactory;
import com.tianhua.amis4j.service.parser.AbstractTemplateParse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Description:
 * date: 2022/5/12
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Component(value = "amisFormElementFactory")
@Slf4j
public class AmisFormElementFactoryImpl extends AbstractTemplateParse implements FormElementFactory {

    @Override
    public String buildInsertForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean) {

        Optional<ParamBean> paramBeanOptional = apiBean.getParamBeanList().stream().filter(paramBean -> paramBean.isModelParam()).findFirst();
        if(!paramBeanOptional.isPresent()){
            log.warn("未找到对应插入API 参数信息");
            return "[]";
        }
        ParamBean paramBean = paramBeanOptional.get();

        StringBuilder dslJsonBuilder = new StringBuilder("[");
        for (FieldBean fieldBean : paramBean.getFieldBeanList()){
            StringBuilder builder = new StringBuilder();
            FieldExtendBean fieldExtendBean = fieldBean.getFieldExtendBean();

            String inputType = preBuildComponent(fieldBean);

            builder.append("\"type\""+": \""+inputType+"\",");
            builder.append("\"name\": \""+fieldBean.getFieldName()+"\",");
            builder.append("\"label\": \""+fieldBean.getFieldDesc()+"\"");
            if (fieldExtendBean.isRequired()){
                builder.append(",\"required\": true");
            }

            //如果不为空或者在插入表单中存在则进行渲染
            if (fieldExtendBean.isRequired() || fieldExtendBean.isInInsertForm()){
                dslJsonBuilder.append("{" + builder.toString()+"},");
            }

        }
        return dslJsonBuilder.substring(0,dslJsonBuilder.length() - 1)+"]";
    }

    @Override
    public String buildUpdateForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean) {

        Optional<ParamBean> paramBeanOptional = apiBean.getParamBeanList().stream().filter(paramBean -> paramBean.isModelParam()).findFirst();
        if(!paramBeanOptional.isPresent()){
            log.warn("未找到对应修改API 参数信息");
            return "[]";
        }
        ParamBean paramBean = paramBeanOptional.get();

        StringBuilder dslJsonBuilder = new StringBuilder("[");
        for (FieldBean fieldBean : paramBean.getFieldBeanList()){
            StringBuilder builder = new StringBuilder();
            FieldExtendBean fieldExtendBean = fieldBean.getFieldExtendBean();
            String inputType = preBuildComponent(fieldBean);

            builder.append("\"type\""+": \""+inputType+"\",");
            builder.append("\"name\": \""+fieldBean.getFieldName()+"\",");
            builder.append("\"label\": \""+fieldBean.getFieldDesc()+"\"");
            if (fieldExtendBean.isRequired()){
                builder.append(",\"required\": true");
            }

            //如果不为空或者在插入表单中存在则进行渲染
            if (fieldExtendBean.isRequired() || fieldExtendBean.isInUpdateForm()){
                dslJsonBuilder.append("{" + builder.toString()+"},");
            }

        }
        return dslJsonBuilder.substring(0,dslJsonBuilder.length() - 1)+"]";
    }

    @Override
    public String buildDetailForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean) {

        ParamBean paramBean = apiBean.getReturnParam();
        if(paramBean == null || !paramBean.isModelParam()){
            log.warn("未找到对应查看API 参数信息");
            return "[]";
        }

        StringBuilder dslJsonBuilder = new StringBuilder("[");
        for (FieldBean fieldBean : paramBean.getFieldBeanList()){
            StringBuilder builder = new StringBuilder();
            builder.append("\"type\""+": \"static\",");
            builder.append("\"name\": \""+fieldBean.getFieldName()+"\",");
            builder.append("\"label\": \""+fieldBean.getFieldDesc()+"\"");
            //如果不为空或者在插入表单中存在则进行渲染
            if (fieldBean.getFieldExtendBean().isInDetailForm()){
                dslJsonBuilder.append("{" + builder.toString()+"},");
            }

        }
        return dslJsonBuilder.substring(0,dslJsonBuilder.length() - 1)+"]";
    }

    @Override
    public String buildPageForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean) {

        ParamBean paramBean = apiBean.getReturnParam();
        if(paramBean == null || !paramBean.isModelParam()){
            log.warn("未找到对应分页API 参数信息");
            return "[]";
        }

        StringBuilder dslJsonBuilder = new StringBuilder("[");
        for (FieldBean fieldBean : paramBean.getFieldBeanList()){
            StringBuilder builder = new StringBuilder();
            builder.append("\"type\""+": \"static\",");
            builder.append("\"name\": \""+fieldBean.getFieldName()+"\",");
            builder.append("\"label\": \""+fieldBean.getFieldDesc()+"\"");
            //如果不为空或者在插入表单中存在则进行渲染
            if (fieldBean.getFieldExtendBean().isInPageForm()){
                dslJsonBuilder.append("{" + builder.toString()+"},");
            }
        }
        return dslJsonBuilder.substring(0,dslJsonBuilder.length() - 1)+"]";
    }


    @Override
    public String preBuildComponent(FieldBean fieldBean){
        FieldExtendBean fieldExtendBean = fieldBean.getFieldExtendBean();
        if(StringUtils.isNotEmpty(fieldExtendBean.getInputType())){
            return fieldExtendBean.getInputType();
        }

        if(fieldBean.getFieldDesc().contains("密码")){
            return AmisComponentEnum.INPUT_PASSWORD.getCompName();
        }
        if(fieldBean.getFieldDesc().contains("邮箱")){
            return AmisComponentEnum.INPUT_EMAIL.getCompName();
        }
        if(fieldBean.getFieldDesc().contains("时间")){
            return AmisComponentEnum.INPUT_DATE_TIME.getCompName();
        }
        if(fieldBean.getFieldDesc().contains("日期")){
            return AmisComponentEnum.INPUT_DATE.getCompName();
        }
        //比如长文本对应的表单元素
        //比如select下的默认值列表

        return AmisComponentEnum.INPUT_TEXT.getCompName();

    }



}
