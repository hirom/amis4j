package com.tianhua.amis4j.service.engine.factory.vue;

import com.coderman.model.meta.bean.PlantUMLApiContextBean;
import com.coderman.model.meta.bean.back.ApiBean;
import com.coderman.model.meta.bean.back.FieldBean;
import com.coderman.model.meta.bean.back.FieldExtendBean;
import com.coderman.model.meta.bean.back.ParamBean;
import com.coderman.model.meta.service.FormElementFactory;
import com.tianhua.amis4j.service.parser.AbstractTemplateParse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Description:
 * date: 2022/5/12
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Component(value = "vueFormElementFactory")
@Slf4j
public class VueFormElementFactoryImpl extends AbstractTemplateParse implements FormElementFactory {

    @Override
    public String buildInsertForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean) {

        Optional<ParamBean> paramBeanOptional = apiBean.getParamBeanList().stream().filter(paramBean -> paramBean.isModelParam()).findFirst();
        if(!paramBeanOptional.isPresent()){
            log.warn("未找到对应插入API 参数信息");
            return "";
        }

        ParamBean paramBean = paramBeanOptional.get();
        StringBuilder dslJsonBuilder = new StringBuilder();
        for (FieldBean fieldBean : paramBean.getFieldBeanList()){
            StringBuilder builder = new StringBuilder();
            FieldExtendBean fieldExtendBean = fieldBean.getFieldExtendBean();
            if(fieldExtendBean.getInputType() == null){
                fieldExtendBean.setInputType("input-text");
            }


            /**
             *  <el-form-item label="考试名称" prop="examName">
             *     <el-input v-model="examInfo.examName"></el-input>
             *  </el-form-item>
             */
            builder.append("<el-form-item label=\""+fieldBean.getFieldDesc()+"\" prop=\""+fieldBean.getFieldName()+"\">\n");
            builder.append("    <el-input v-model=\"addForm."+fieldBean.getFieldName()+"\"></el-input>\n");
            builder.append("</el-form-item>\n");
            builder.append("\n");
            //如果不为空或者在插入表单中存在则进行渲染
            if (fieldExtendBean.isRequired() || fieldExtendBean.isInInsertForm()){
                dslJsonBuilder.append( builder.toString());
            }

        }
        return dslJsonBuilder.toString();
    }

    @Override
    public String buildUpdateForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean) {

        Optional<ParamBean> paramBeanOptional = apiBean.getParamBeanList().stream().filter(paramBean -> paramBean.isModelParam()).findFirst();
        if(!paramBeanOptional.isPresent()){
            log.warn("未找到对应修改API 参数信息");
            return "";
        }
        ParamBean paramBean = paramBeanOptional.get();

        StringBuilder dslJsonBuilder = new StringBuilder();
        for (FieldBean fieldBean : paramBean.getFieldBeanList()){
            StringBuilder builder = new StringBuilder();
            FieldExtendBean fieldExtendBean = fieldBean.getFieldExtendBean();
            if(fieldExtendBean.getInputType() == null){
                fieldExtendBean.setInputType("input-text");
            }

            /**
             *  <el-form-item label="考试名称" prop="examName">
             *     <el-input v-model="examInfo.examName"></el-input>
             *  </el-form-item>
             */
            builder.append("<el-form-item label=\""+fieldBean.getFieldDesc()+"\" prop=\""+fieldBean.getFieldName()+"\">\n");
            builder.append("    <el-input v-model=\"addForm."+fieldBean.getFieldName()+"\"></el-input>\n");
            builder.append("</el-form-item>\n");
            builder.append("\n");
            //如果不为空或者在插入表单中存在则进行渲染
            if (fieldExtendBean.isRequired() || fieldExtendBean.isInInsertForm()){
                dslJsonBuilder.append(builder.toString());
            }

        }
        return dslJsonBuilder.toString();
    }

    @Override
    public String buildDetailForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean) {

        ParamBean paramBean = apiBean.getReturnParam();
        if(paramBean == null || !paramBean.isModelParam()){
            log.warn("未找到对应查看API 参数信息");
            return "[]";
        }

        StringBuilder dslJsonBuilder = new StringBuilder("[");
        for (FieldBean fieldBean : paramBean.getFieldBeanList()){
            StringBuilder builder = new StringBuilder();
            builder.append("\"type\""+": \"static\",");
            builder.append("\"name\": \""+fieldBean.getFieldName()+"\",");
            builder.append("\"label\": \""+fieldBean.getFieldDesc()+"\"");
            //如果不为空或者在插入表单中存在则进行渲染
            if (fieldBean.getFieldExtendBean().isInDetailForm()){
                dslJsonBuilder.append("{" + builder.toString()+"},");
            }

        }
        return dslJsonBuilder.substring(0,dslJsonBuilder.length() - 1)+"]";
    }

    @Override
    public String buildPageForm(ApiBean apiBean, PlantUMLApiContextBean plantUMLApiContextBean) {

        ParamBean paramBean = apiBean.getReturnParam();
        if(paramBean == null || !paramBean.isModelParam()){
            log.warn("未找到对应分页API 参数信息");
            return "";
        }

        StringBuilder dslJsonBuilder = new StringBuilder();
        for (FieldBean fieldBean : paramBean.getFieldBeanList()){
            StringBuilder builder = new StringBuilder();

            /**
             *
             *   <el-table-column align="center" prop="username" label="用户名" />
             */
            builder.append("<el-table-column align=\"center\" prop=\""+fieldBean.getFieldName()+"\" label=\""+fieldBean.getFieldDesc()+"\" />\n");
            //如果不为空或者在插入表单中存在则进行渲染
            if (fieldBean.getFieldExtendBean().isInPageForm()){
                dslJsonBuilder.append(builder.toString());
            }
        }
        return dslJsonBuilder.toString();
    }

    @Override
    public String preBuildComponent(FieldBean fieldBean) {
        return null;
    }
}
