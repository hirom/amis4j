package com.tianhua.amis4j.service.engine.factory.amis;

import com.coderman.model.meta.bean.PlantUMLApiContextBean;
import com.coderman.model.meta.bean.back.FieldBean;
import com.coderman.model.meta.bean.container.ContainerBean;
import com.coderman.model.meta.bean.dsl.CodeFactoryService;
import com.coderman.model.meta.bean.dsl.amis.AmisContainerEnum;
import com.coderman.model.meta.service.FormElementFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Description:
 * date: 2022/4/29
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Component(value = "amisTemplateDslJsonFactory")
public class AmisCodeJsonFactoryImpl implements CodeFactoryService {

    @Resource(name = "amisFormElementFactory")
    private FormElementFactory amisFormElementFactory;

    @Override
    public String buildWebCode(ContainerBean containerBean, PlantUMLApiContextBean plantUMLApiContextBean) {
        if(containerBean.getContainer() == null){
            return containerBean.buildRequestDslJson();
        }
        String containerName = containerBean.getContainer().getContainerName();
        if(AmisContainerEnum.isInsertForm(containerName)){
            return amisFormElementFactory.buildInsertForm(containerBean.getApiBean(), plantUMLApiContextBean);
        }

        if(AmisContainerEnum.isPageList(containerName)){
            return amisFormElementFactory.buildPageForm(containerBean.getApiBean(),plantUMLApiContextBean);
        }

        if(AmisContainerEnum.isUpdateForm(containerName)){
            return amisFormElementFactory.buildUpdateForm(containerBean.getApiBean(),plantUMLApiContextBean);
        }

        if(AmisContainerEnum.isDetailForm(containerName)){
            return amisFormElementFactory.buildDetailForm(containerBean.getApiBean(),plantUMLApiContextBean);
        }
        return containerBean.buildRequestDslJson();
    }

    @Override
    public String preBuildComponent(FieldBean fieldBean) {
        return amisFormElementFactory.preBuildComponent(fieldBean);
    }


}
