package com.tianhua.amis4j.service.parser;

import com.coderman.model.meta.annotations.Template;
import com.coderman.model.meta.bean.back.ApiBean;
import com.coderman.model.meta.bean.back.TemplateParseContextBean;
import com.coderman.model.meta.bean.back.WebCodeBean;
import com.coderman.model.meta.bean.dsl.ICURDParse;
import com.coderman.model.meta.enums.CodeFileTypeEnum;
import com.coderman.model.meta.enums.CodeLevelEnum;
import com.tianhua.amis4j.service.admin.WebCodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Description:对modulecurd进行解析
 * date: 2022/5/11
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Slf4j
@Service
@Template(name = "modulecrud",webArchName = "vueelementui")
public class VueCurdParseImpl extends AbstractTemplateParse implements ICURDParse {

    @Autowired
    private WebCodeService webDslConfigService;

    @Override
    public void parse(TemplateParseContextBean templateParseContextBean) {

        String moduleTitle = getModuleTitle(templateParseContextBean.getModuleCode(),templateParseContextBean.getProjectSnapShotBean());
        String updateApi = getUpdateApi(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String pageApi = getPageApi(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String deleteApi = getDeleteApi(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String getInsertForm = getInsertForm(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String getDetailForm = getDetailForm(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String getUpdateForm = getUpdateForm(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());
        String getPageForm = getPageForm(templateParseContextBean.getModuleCode(), templateParseContextBean.getProjectSnapShotBean());

        String templateContent = templateParseContextBean.getWebCodeTemplateBean().getTemplateContent();
        String newContent = templateContent.replace("$moduleTitle",moduleTitle)
                .replace("$insertForm",getInsertForm)
                .replace("$pageApi",pageApi)
                .replace("$detailForm",getDetailForm)
                .replace("$updateApi",updateApi)
                .replace("$updateForm",getUpdateForm)
                .replace("$pageForm",getPageForm)
                .replace("$deleteApi",deleteApi);


        log.info("解析后的content = "+newContent);

        WebCodeBean webDslConfigBean = new WebCodeBean();
        webDslConfigBean.setWebDslJson(newContent);
        webDslConfigBean.setModuleCode(templateParseContextBean.getModuleCode());
        webDslConfigBean.setCodeLevel(CodeLevelEnum.MODULE.getLevel());
        webDslConfigBean.setApiUrl("");
        webDslConfigBean.setSinglePageHtml("");
        webDslConfigBean.setContentType(CodeFileTypeEnum.VUE.getCodeType());
        webDslConfigBean.setProjectCode(templateParseContextBean.getProjectCode());
        webDslConfigBean.setVersion("V1.0");
        //持久化模块级的代码内容
        webDslConfigService.saveWebDslConfig(webDslConfigBean);

        buildApiJs(templateParseContextBean);
    }

    /**
     * 构建apijs文件
     * @param templateParseContextBean
     */
    private void buildApiJs(TemplateParseContextBean templateParseContextBean){
        List<ApiBean> apiBeanList = templateParseContextBean.getProjectSnapShotBean().getApiBeanListMap().get(templateParseContextBean.getModuleCode());

        StringBuilder jsBuilder = new StringBuilder("");
        for (ApiBean apiBean : apiBeanList){
            StringBuilder apiBuilder = new StringBuilder("//"+apiBean.getApiDoc()+"\n");
            String methodName = apiBean.getApiUrl().replace(templateParseContextBean.getModuleCode(),"").replace("/","");
            apiBuilder.append("export function "+methodName+"(data) {\n");
            apiBuilder.append("  return request({\n");
            apiBuilder.append("    url: '"+apiBean.getApiUrl()+"',\n");
            apiBuilder.append("    method: '"+apiBean.getMethodType()+"',\n");
            //todo restful支持
            if(apiBean.isPost() || apiBean.isPut()){
                apiBuilder.append("    data: data\n");
            }

            if(apiBean.isGet()){
                apiBuilder.append("    param: data\n");
            }
            apiBuilder.append("  })\n");
            apiBuilder.append("}\n\n\n");
            jsBuilder.append(apiBuilder.toString());
        }

        WebCodeBean webDslConfigBean = new WebCodeBean();
        webDslConfigBean.setWebDslJson(jsBuilder.toString());
        webDslConfigBean.setModuleCode(templateParseContextBean.getModuleCode());
        webDslConfigBean.setCodeLevel(CodeLevelEnum.MODULE.getLevel());
        webDslConfigBean.setApiUrl("");
        webDslConfigBean.setSinglePageHtml("");
        webDslConfigBean.setContentType(CodeFileTypeEnum.JAVASCRIPT.getCodeType());
        webDslConfigBean.setProjectCode(templateParseContextBean.getProjectCode());
        webDslConfigBean.setVersion("V1.0");
        //持久化模块级的代码内容
        webDslConfigService.saveWebDslConfig(webDslConfigBean);

    }


}
