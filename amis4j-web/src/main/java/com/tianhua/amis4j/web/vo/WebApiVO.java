package com.tianhua.amis4j.web.vo;

import lombok.Data;

import java.util.Date;
import java.math.BigDecimal;

/**
* @Description:API数据表VO类
* @Author:shenshuai
* @CreateTime:2022-04-15 16:23:32
* @version v1.0
*/
@Data
public class WebApiVO{

	/**   主键 **/
	private Long id;
	/**  项目名称 **/
	private String projectCode;
	/**  项目应用名 **/
	private String moduleCode;
	/**  api信息 **/
	private String apiUrl;
	/**  api描述 **/
	private String apiDoc;
	/**  请求方式 **/
	private String methodType;
	/**  请求json数据演示 **/
	private String requestJsonData;
	/**  响应json数据演示 **/
	private String responseJsonData;
	/**  请求参数元信息 **/
	private String requestParam;
	/**  创建时间 **/
	private Date dateCreate;
	/**  修改时间 **/
	private Date dateUpdate;
	/**  修改人 **/
	private Long updateUserId;
	/**  创建人 **/
	private Long createUserId;

	/**
	 * web代码模版名称---走持久化-动态模版配置
	 */
	public String templateName;


	public void init(){
		this.dateCreate = new Date();
		this.dateUpdate = new Date();
		this.updateUserId = 1L;
		this.createUserId = 1L;
	}
}