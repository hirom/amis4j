package com.tianhua.amis4j.web.convert;

import com.coderman.model.meta.bean.back.WebCodeTemplateBean;
import com.tianhua.amis4j.web.vo.WebCodeTemplateVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Description:
 * date: 2022/4/19
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Mapper
public interface WebCodeTemplateConfigConvert {
    WebCodeTemplateConfigConvert INSTANCE = Mappers.getMapper(WebCodeTemplateConfigConvert.class);

    WebCodeTemplateBean convert2bean(WebCodeTemplateVO webArchTemplateVO);

    WebCodeTemplateVO convert2vo(WebCodeTemplateBean webArchTemplateBean);

    List<WebCodeTemplateVO> convert2beans(List<WebCodeTemplateBean> webArchTemplateBeanList);

}
