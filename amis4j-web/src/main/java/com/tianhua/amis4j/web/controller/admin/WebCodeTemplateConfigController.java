package com.tianhua.amis4j.web.controller.admin;

import com.alibaba.fastjson.JSON;
import com.coderman.model.meta.bean.back.WebCodeTemplateBean;
import com.coderman.model.meta.bean.support.PageBean;
import com.coderman.utils.response.ResultDataDto;
import com.coderman.utils.response.ResultDto;
import com.tianhua.amis4j.service.admin.WebCodeTemplateConfigService;
import com.tianhua.amis4j.web.convert.WebCodeTemplateConfigConvert;
import com.tianhua.amis4j.web.vo.WebCodeTemplateVO;
import com.tianhua.amis4j.web.vo.query.WebCodeQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
* @Description:web前端应用框架低代码模版配置控制层
* @Author:shenshuai
* @CreateTime:2022-04-15 16:23:32
* @version v1.0
*/
@RestController
public class WebCodeTemplateConfigController extends BaseController{
	
	protected Logger logger = LoggerFactory.getLogger(WebCodeTemplateConfigController.class);

	@Autowired
	private WebCodeTemplateConfigService webCodeTemplateConfigService;

	/**
     * @Description:新增dsljson配置
     * @version v1.0
     * @param webCodeTemplateVO
     * @return ResultDto
     */
    @RequestMapping(value = "/webCodeTemplateConfig/add",method = RequestMethod.POST)
    public ResultDto add(@RequestBody WebCodeTemplateVO webCodeTemplateVO){
		webCodeTemplateConfigService.saveWebArchTemplateConfig(WebCodeTemplateConfigConvert.INSTANCE.convert2bean(webCodeTemplateVO));
		return ResultDto.success();
    }

	/**
	 * @Description:修改dsljson配置
	 * @version v1.0
	 * @param webArchTemplateVO
	 * @return ResultDto
	 */
    @RequestMapping(value = "/webCodeTemplateConfig/update/{id}",method = RequestMethod.POST)
	public ResultDto update(@PathVariable(value = "id")Long id, @RequestBody WebCodeTemplateVO webArchTemplateVO){
		WebCodeTemplateBean webCodeTemplateBean = WebCodeTemplateConfigConvert.INSTANCE.convert2bean(webArchTemplateVO);
		webCodeTemplateBean.setId(id);
		webCodeTemplateConfigService.updateWebArchTemplateConfig(webCodeTemplateBean);
		return ResultDto.success();
	}

	/**
	 * @Description:修改dsljson配置
	 * @version v1.0
	 * @param id
	 * @return ResultDto
	 */
	@RequestMapping(value = "/webCodeTemplateConfig/getById/{id}",method = RequestMethod.GET)
	public ResultDataDto getById(@PathVariable(value = "id")Long id){
		WebCodeTemplateBean webCodeTemplateBean = webCodeTemplateConfigService.getById(id);
		WebCodeTemplateVO webCodeTemplateVO = WebCodeTemplateConfigConvert.INSTANCE.convert2vo(webCodeTemplateBean);
		return ResultDataDto.success(webCodeTemplateVO);
	}


	/**
	 * @Description:分页获取模块dsljson记录
	 * @version v1.0
	 * @return ResultDataDto
	 */
	@GetMapping("/webCodeTemplateConfig/pagelist")
	public ResultDataDto getPage(WebCodeQuery query){
		logger.info("WebDslConfigQuery pageVo = "+ JSON.toJSONString(query));
		PageBean pageBean = query.getPageBean();
		List<WebCodeTemplateVO> webCodeTemplateVOList = WebCodeTemplateConfigConvert.INSTANCE.convert2beans(webCodeTemplateConfigService.selectPage(pageBean).getRows());


		query.setRows(webCodeTemplateVOList);
		query.setCount(pageBean.getCount());
		return ResultDataDto.success(query);
	}


	@GetMapping("/webCodeTemplateConfig/getByWebAppName")
	public ResultDataDto search(String webAppName){
		List<WebCodeTemplateBean> webCodeTemplateBeanList = webCodeTemplateConfigService.getByWebAppName(webAppName);
		List<WebCodeTemplateVO> webCodeTemplateVOList = WebCodeTemplateConfigConvert.INSTANCE.convert2beans(webCodeTemplateBeanList);
		return ResultDataDto.success(wrapperCodeTemplate(webCodeTemplateVOList));
	}

	@GetMapping("/webCodeTemplateConfig/varlist")
	public ResultDataDto getTemplateVarList(){
		webCodeTemplateConfigService.getTemplateVarList();
		return ResultDataDto.success(webCodeTemplateConfigService.getTemplateVarList());
	}

}
