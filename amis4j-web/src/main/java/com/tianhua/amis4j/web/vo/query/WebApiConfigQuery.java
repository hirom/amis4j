package com.tianhua.amis4j.web.vo.query;

import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.web.vo.PageVO;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Description:
 * date: 2022/4/25
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class WebApiConfigQuery extends PageVO {
    private String  projectCode;

    private String moduleCode;

    private String apiUrl;



    public PageBean getPageBean(){
        PageBean pageBean = super.getPageBean();
        Map<String,Object> query = new HashMap<>();
        if(StringUtils.isNotEmpty(this.moduleCode)){
            query.put("moduleCode",moduleCode);
        }
        if(StringUtils.isNotEmpty(this.projectCode)){
            query.put("projectCode",projectCode);
        }
        if(StringUtils.isNotEmpty(this.apiUrl)){
            query.put("apiUrl",apiUrl);
        }

        pageBean.setQuery(query);
        return pageBean;
    }
}
