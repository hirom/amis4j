package com.tianhua.amis4j.web.vo;

import com.alibaba.fastjson.JSON;
import com.coderman.model.meta.bean.back.FieldBean;
import lombok.Data;
import org.apache.commons.collections.CollectionUtils;

import java.util.Date;
import java.math.BigDecimal;
import java.util.List;

/**
* @Description:dto/vo参数表VO类
* @Author:shenshuai
* @CreateTime:2022-04-15 16:23:32
* @version v1.0
*/
@Data
public class WebApiParamVO{

	/**   主键 **/
	private Long id;
	/**  项目名称 **/
	private String projectCode;
	/**  项目应用名 **/
	private String moduleCode;
	/**  参数类名 **/
	private String paramClassName;
	/**  参数类描述 **/
	private String paramClassDesc;
	/**  参数属性集合json **/
	private String paramFieldJson;
	/**  创建时间 **/
	private Date dateCreate;
	/**  修改时间 **/
	private Date dateUpdate;
	/**  修改人 **/
	private Long updateUserId;
	/**  创建人 **/
	private Long createUserId;


	/**
	 * 属性列表
	 */
	List<FieldBean> fieldBeanList;


	public void init(){
		this.dateCreate = new Date();
		this.dateUpdate = new Date();
		this.updateUserId = 1L;
		this.createUserId = 1L;
		if(CollectionUtils.isNotEmpty(this.fieldBeanList)){
			this.paramFieldJson = JSON.toJSONString(this.fieldBeanList);
		}
	}
}