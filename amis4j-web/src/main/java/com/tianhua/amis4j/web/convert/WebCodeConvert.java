package com.tianhua.amis4j.web.convert;

import com.coderman.model.meta.bean.back.WebCodeBean;
import com.tianhua.amis4j.web.vo.WebCodeVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Description:
 * date: 2022/4/19
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Mapper
public interface WebCodeConvert {
    WebCodeConvert INSTANCE = Mappers.getMapper(WebCodeConvert.class);

    WebCodeBean convert2bean(WebCodeVO webDslConfigVO);

    WebCodeVO convert2vo(WebCodeBean webDslConfigBean);

    List<WebCodeVO> convert2beans(List<WebCodeBean> webDslConfigBeans);

}
