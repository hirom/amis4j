package com.tianhua.amis4j.web.vo;

import lombok.Data;

import java.util.List;

/**
 * Description:
 * date: 2022/5/18
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Data
public class CreateCodeConfigVO {
    /**   主键 **/
    private Long id;
    /**  项目编码 **/
    private String projectCode;

    /**  模块编码 **/
    private String moduleCode;

    /**
     * 使用的web框架
     */
    private String webAppName;

    private List<WebApiVO> apiList;


}
