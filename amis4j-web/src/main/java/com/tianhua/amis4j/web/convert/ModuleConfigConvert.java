package com.tianhua.amis4j.web.convert;

import com.coderman.model.meta.bean.back.CreateCodeConfigBean;
import com.coderman.model.meta.bean.back.ModuleBean;
import com.tianhua.amis4j.web.vo.CreateCodeConfigVO;
import com.tianhua.amis4j.web.vo.ModuleConfigVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Description:
 * date: 2022/4/19
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Mapper
public interface ModuleConfigConvert {
    ModuleConfigConvert INSTANCE = Mappers.getMapper(ModuleConfigConvert.class);

    ModuleBean convert2bean(ModuleConfigVO configVO);

    ModuleConfigVO convert2vo(ModuleBean moduleBean);

    List<ModuleConfigVO> convert2beans(List<ModuleBean> projectBeanList);

    CreateCodeConfigBean convert2bean(CreateCodeConfigVO createCodeConfigVO);

}
