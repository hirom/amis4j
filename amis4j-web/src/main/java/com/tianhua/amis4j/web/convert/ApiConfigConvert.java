package com.tianhua.amis4j.web.convert;

import com.coderman.model.meta.bean.back.ApiBean;
import com.coderman.model.meta.bean.back.ParamBean;
import com.tianhua.amis4j.web.vo.WebApiVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * Description:
 * date: 2022/4/19
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
@Mapper
public interface ApiConfigConvert {
    ApiConfigConvert INSTANCE = Mappers.getMapper(ApiConfigConvert.class);

    ApiBean convert2bean(WebApiVO apiVO);

    WebApiVO convert2vo(ApiBean apiBean);

    List<WebApiVO> convert2beans(List<ApiBean> apiBeanList);

}
