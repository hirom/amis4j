package com.tianhua.amis4j.web.vo;


import lombok.Data;

/**
* @Description:项目配置VO类
* @Author:shenshuai
* @CreateTime:2022-04-15 16:23:32
* @version v1.0
*/
@Data
public class WebCodeVO {

	/**   主键 **/
	private Long id;
	/**  项目编码 **/
	private String projectCode;
	/**  模块编码 **/
	private String moduleCode;
	/**  api信息 **/
	private String apiUrl;
	/**  api信息 **/
	private String webDslJson;
	/**  单页面htmldemo **/
	private String singlePageHtml;
	/**  版本号 **/
	private String version;


	/**
	 * 代码文件内容格式
	 * JavaScript
	 * JSON
	 *
	 */
	private String contentType;


	/**
	 * 代码级别
	 * API
	 * MODULe
	 */
	private String codeLevel;
}