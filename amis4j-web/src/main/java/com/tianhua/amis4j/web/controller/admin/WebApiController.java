package com.tianhua.amis4j.web.controller.admin;

import com.alibaba.fastjson.JSON;
import com.coderman.model.meta.bean.back.ApiBean;
import com.coderman.model.meta.bean.back.ModuleBean;
import com.coderman.model.meta.bean.support.PageBean;
import com.tianhua.amis4j.service.admin.ApiConfigService;
import com.tianhua.amis4j.web.convert.ApiConfigConvert;
import com.tianhua.amis4j.web.convert.ModuleConfigConvert;
import com.tianhua.amis4j.web.vo.query.WebApiConfigQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tianhua.amis4j.web.vo.WebApiVO;


import com.coderman.utils.response.ResultDataDto;
import com.coderman.utils.response.ResultDto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
* @Description:API数据表控制层
* @Author:shenshuai
* @CreateTime:2022-04-15 16:23:32
* @version v1.0
*/
@RestController
public class WebApiController{
	
	protected Logger logger = LoggerFactory.getLogger(WebApiController.class);

	@Autowired
	private ApiConfigService apiConfigService;

	/**
     * @Description:新增API
     * @version v1.0
     * @param webApiVO
     * @return ResultDto
     */
    @RequestMapping(value = "/webApi/add",method = RequestMethod.POST)
    public ResultDto add(@RequestBody WebApiVO webApiVO){
		webApiVO.init();
		apiConfigService.saveApiConfig(ApiConfigConvert.INSTANCE.convert2bean(webApiVO));

		return ResultDto.success();
    }

	/**
	 * @Description:修改API数据表
	 * @version v1.0
	 * @param webApiVO
	 * @return ResultDto
	 */
    @RequestMapping(value = "/webApi/update/{id}",method = RequestMethod.POST)
	public ResultDto update(@PathVariable(value = "id")Long id, @RequestBody WebApiVO webApiVO){
		webApiVO.init();
		ApiBean apiBean = ApiConfigConvert.INSTANCE.convert2bean(webApiVO);
		apiBean.setId(id);
		apiConfigService.updateApiConfig(apiBean);
		return ResultDto.success();
	}


	/**
	 * @Description:修改API数据表
	 * @version v1.0
	 * @param id
	 * @return ResultDto
	 */
	@RequestMapping(value = "/webApi/delete/{id}",method = RequestMethod.DELETE)
	public ResultDto delete(@PathVariable(value = "id")Long id){
		apiConfigService.delete(id);
		return ResultDto.success();
	}


	/**
	 * @Description:分页获取API数据表记录
	 * @version v1.0
	 * @return ResultDataDto
	 */
	@GetMapping("/webApi/pagelist")
	public ResultDataDto getPage(WebApiConfigQuery query){
		logger.info("ModuleConfigQuery pageVo = "+ JSON.toJSONString(query));
		PageBean pageBean = query.getPageBean();
		List<WebApiVO> apiVOList = ApiConfigConvert.INSTANCE.convert2beans(apiConfigService.selectPage(pageBean).getRows());
		query.setRows(apiVOList);
		query.setCount(pageBean.getCount());
		return ResultDataDto.success(query);
	}

	@RequestMapping(value = "/webApi/getById/{id}",method = RequestMethod.GET)
	public ResultDataDto getById(@PathVariable(name = "id") Long id){
		ApiBean apiBean = apiConfigService.getById(id);
		return ResultDataDto.success(ApiConfigConvert.INSTANCE.convert2vo(apiBean));
	}


}
