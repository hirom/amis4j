package com.tianhua.amis4j.web.controller.admin;

import com.tianhua.amis4j.web.vo.ModuleConfigVO;
import com.tianhua.amis4j.web.vo.OptionsVO;
import com.tianhua.amis4j.web.vo.ProjectConfigVO;
import com.tianhua.amis4j.web.vo.WebCodeTemplateVO;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

/**
 * Description:
 * date: 2022/5/17
 *
 * @author fanchunshuai
 * @version 1.0.0
 * @since JDK 1.8
 */
public abstract class BaseController {

    /**
     * 包装option适配amis框架
     * @param projectConfigVOList
     * @return
     */
    public OptionsVO wrapperProject(List<ProjectConfigVO> projectConfigVOList){
        OptionsVO optionsVO = new OptionsVO();
        if(CollectionUtils.isEmpty(projectConfigVOList)){
            return optionsVO;
        }
        projectConfigVOList.stream().forEach(projectConfigVO -> {
            optionsVO.addOptionItem(projectConfigVO.getProjectDesc(), projectConfigVO.getProjectCode());
        });
        return optionsVO;
    }

    /**
     * 包装option适配amis框架
     * @param moduleConfigVOList
     * @return
     */
    public OptionsVO wrapperModule(List<ModuleConfigVO> moduleConfigVOList){
        OptionsVO optionsVO = new OptionsVO();
        if(CollectionUtils.isEmpty(moduleConfigVOList)){
            return optionsVO;
        }
        moduleConfigVOList.stream().forEach(projectConfigVO -> {
            optionsVO.addOptionItem(projectConfigVO.getModuleDesc(), projectConfigVO.getModuleCode());
        });
        return optionsVO;
    }



    /**
     * 包装option适配amis框架
     * @param webCodeTemplateVOList
     * @return
     */
    public OptionsVO wrapperCodeTemplate(List<WebCodeTemplateVO> webCodeTemplateVOList){
        OptionsVO optionsVO = new OptionsVO();
        if(CollectionUtils.isEmpty(webCodeTemplateVOList)){
            return optionsVO;
        }
        webCodeTemplateVOList.stream().forEach(webCodeTemplateVO -> {
            optionsVO.addOptionItem(webCodeTemplateVO.getDesc(), webCodeTemplateVO.getTemplateName());
        });
        return optionsVO;
    }

}
