{
"title": "${insertApiTitle}",
"body": [{
"type": "form",
"mode": "horizontal",
"api": "${insertApi}",
"actions": [{
"type": "submit",
"label": "提交",
"primary": true
}],
"collapsable": true,
"title":  "${insertApiTitle}",
"body": [

<#list insertApiParam as param>
    {
    "type": "${param.inputType}",
    "name": "${param.fieldName}",
    "label": "${param.fieldDesc}"
    <#if (param.required)??>
        ,"required": true
    </#if>
    },{
    "type": "divider"
    },
</#list>
    {
        "type": "divider"
    }
]
}]
}