{
"type": "page",
"title": "${detailApiTitle}",
"remark": null,
"toolbar": [
{
"type": "button",
"actionType": "link",
"link": "/api/list",
"label": "返回列表"
}
],
"body": [
    {
        "type": "page",
        "initApi": "${detailApi}/${params.id}",
        "controls": [
<#list detailApiParam as param>
    {
    "type": "static",
    "name": "${param.fieldName}",
    "label": "${param.fieldDesc}"
    },{
    "type": "divider"
    },
</#list>
{
"type": "divider"
}
        ]
    }
]
}
