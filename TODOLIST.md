1. API plantUML的增量更新
2. 支持json,vue等通用模版变量替换
3. 修改web应用框架的使用，以项目为维度来构建，后期不允许随便更改
4. 一键式生成项目代码优化


未来规划：
1. 支持复杂表单的构建，提升代码生成的丰富度
2. 支持项目模型中的枚举识别和管理，提升代码生成完整度
3. 支持表字段结构信息与参数信息结合提升代码生成精确度
4. 构建除amis的其他web框架代码的基本CURD模版，提升代码生成广度。
5. 完善不同web框架的组件，容器的模型定义
6. 支持plantUML文件增量归档







api:xxxx1
alias:a
params.kv

api:xxxx2
alias:b

json:
  a.x = a.x
  b.w = b.w
  m = a.y.x


select user.getByid?id=1 from user-plat
select user.getByid?id=1 from user-plat

select * from user-plat where userId=1
update user-plat set a=1,b=2 where api=user.post

insert user-plat.user.save values (
 json
)


select * from env where serviceName='user-plat' and ip = '222' and namespace=''
env:
  ip
  port
  namespace
  group
  envType=dev,test,stable,pre,pro,local
  serviceName 


select * from api where serviceName='user-plat'
api:
 api
 request
 methodType
 response
 version

select * from log where serviceName='user-plat' and api=user
select * from trace where serviceName='user-plat'  and traceId='adadf'


SqlMesh


select * from mq where topic=
select * from mq where topic=a and tag=b
select * from mq where provider='user-plat'
 service:topic:tag:desc:msgbody

select * from mq where consumer='user-plat'
 service1:topic:tag:desc:msgbody
 service2:topic:tag:desc:

insert mq.xxx.xxx where (msgbody) -->发送一条消息

select * from msg where consumer='user-plat' and topic=a and tag=b
msg:
 msgbody
 status
 time
 consumerip
 


show api from service
show env from service

show topic from mq

show kv from nacos



webmanager--adapter

domain

service

core

select * from nacos where dataId= and groupId=


infrast

